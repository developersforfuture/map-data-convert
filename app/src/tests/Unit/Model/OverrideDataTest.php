<?php

namespace App\Tests\Unit\Model;

use App\Model\DataObjectInterface;
use App\Model\OverrideData;
use PHPUnit\Framework\TestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class OverrideDataTest extends TestCase
{
    /**
     * @dataProvider getValidData
     */
    public function testBuildValues(array $data, array $colConfig, array $expectedResult)
    {
        /** @var OverrideData $overrideDataObject */
        $overrideDataObject = OverrideData::buildByOrderedArrayData($data, $colConfig);

        $this->assertSame($expectedResult, $overrideDataObject->getValuesByGeneralCols());
    }

    public function getValidData()
    {
        return [
            [
                [
                    '123',
                    'info@developersforfuture.org',
                    'Developer',
                    'y',
                    'y',
                    'Geermany',
                    'Ansbach',
                    'Martin-Luther-Platz',
                    '14 Ur',
                    '24/05/2019',
                    'Once Only',
                    'DevelopersForFuture',
                    '+4912345',
                    'Some Notes',
                    'Strike',
                    'https::/developersforfuture.org',
                    'y',
                    'y',
                    'OrgType',
                    'Some comments',
                    'Martin-Lutherplatz, Ansbach',
                    '1234',
                    '5678',
                    '14:00',
                    'info@developersforfuture.org',
                    '300',
                ],
                [
                    DataObjectInterface::COL_TIMESTAMP,
                    DataObjectInterface::COL_EMAIL_ADDRESS,
                    DataObjectInterface::COL_NAME,
                    DataObjectInterface::COL_REGISTRATION_CONSENT,
                    DataObjectInterface::COL_SPOKESPERSON_CONSENT,
                    DataObjectInterface::COL_COUNTRY,
                    DataObjectInterface::COL_TOWN,
                    DataObjectInterface::COL_ADDRESS,
                    DataObjectInterface::COL_TIME,
                    DataObjectInterface::COL_DATE,
                    DataObjectInterface::COL_FREQUENCY,
                    DataObjectInterface::COL_ORGANIZATION,
                    DataObjectInterface::COL_PHONE_NUMBER,
                    DataObjectInterface::COL_NOTES,
                    DataObjectInterface::COL_EVENT_TYPE,
                    DataObjectInterface::COL_LINK_TO_EVENT,
                    DataObjectInterface::COL_EVENT_CONSENT,
                    DataObjectInterface::COL_APPROVE,
                    DataObjectInterface::COL_ORGANIZATION_TYPE,
                    DataObjectInterface::COL_COMMENT,
                    DataObjectInterface::COL_LOCALIZATION,
                    DataObjectInterface::COL_LATITUDE,
                    DataObjectInterface::COL_LONGITUDE,
                    DataObjectInterface::COL_APPROVAL_TIME,
                    DataObjectInterface::COL_APPROVAL_EMAIL,
                    DataObjectInterface::COL_NUMBER_OF_PEOPLE,
                ],
                [
                    DataObjectInterface::COL_TIMESTAMP => '123',
                    DataObjectInterface::COL_EMAIL_ADDRESS => 'info@developersforfuture.org',
                    DataObjectInterface::COL_NAME => 'Developer',
                    DataObjectInterface::COL_REGISTRATION_CONSENT => 'y',
                    DataObjectInterface::COL_SPOKESPERSON_CONSENT => 'y',
                    DataObjectInterface::COL_COUNTRY => 'Geermany',
                    DataObjectInterface::COL_TOWN => 'Ansbach',
                    DataObjectInterface::COL_ADDRESS => 'Martin-Luther-Platz',
                    DataObjectInterface::COL_TIME => '14 Ur',
                    DataObjectInterface::COL_DATE => '24/05/2019',
                    DataObjectInterface::COL_FREQUENCY => 'Once Only',
                    DataObjectInterface::COL_ORGANIZATION => 'DevelopersForFuture',
                    DataObjectInterface::COL_PHONE_NUMBER => '+4912345',
                    DataObjectInterface::COL_NOTES => 'Some Notes',
                    DataObjectInterface::COL_EVENT_TYPE => 'Strike',
                    DataObjectInterface::COL_LINK_TO_EVENT => 'https::/developersforfuture.org',
                    DataObjectInterface::COL_EVENT_CONSENT => 'y',
                    DataObjectInterface::COL_APPROVE => 'y',
                    DataObjectInterface::COL_ORGANIZATION_TYPE => 'OrgType',
                    DataObjectInterface::COL_COMMENT => 'Some comments',
                    DataObjectInterface::COL_LOCALIZATION => 'Martin-Lutherplatz, Ansbach',
                    DataObjectInterface::COL_LATITUDE => '1234',
                    DataObjectInterface::COL_LONGITUDE => '5678',
                    DataObjectInterface::COL_APPROVAL_TIME => '14:00',
                    DataObjectInterface::COL_APPROVAL_EMAIL => 'info@developersforfuture.org',
                    DataObjectInterface::COL_NUMBER_OF_PEOPLE => '300',
                ],
            ],
        ];
    }
}
