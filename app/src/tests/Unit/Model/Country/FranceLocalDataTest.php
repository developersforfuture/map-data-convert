<?php

namespace App\Tests\Unit\Model\Country;

use App\Model\Country\FranceLocalData;
use App\Model\DataObjectInterface;
use App\Model\OverrideData;
use PHPUnit\Framework\TestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class FranceLocalDataTest extends TestCase
{
    /**
     * @dataProvider getValidData
     */
    public function testBuildValues(array $data, array $colConfig, array $expectedResult)
    {
        /** @var OverrideData $overrideDataObject */
        $overrideDataObject = FranceLocalData::buildByOrderedArrayData($data, $colConfig);

        $this->assertEquals($expectedResult, $overrideDataObject->getValuesByGeneralCols());
    }


    public function getValidData()
    {
        return [
            [
                [
                    'Albertville',
                    'Louise Collombier',
                    'yfc.albertville@gmail.com',
                    'https://www.instagram.com/youth_for_climate_albertville/',
                    'Once only',
                    '24/05/2019',
                    'Devant la mairie',
                    '14h',
                    '5° 27′ 14″ est',
                    '43° 31′ 52″ nord,',
                    'France',
                ],
                [
                    DataObjectInterface::COL_TOWN,
                    DataObjectInterface::COL_NAME,
                    DataObjectInterface::COL_EMAIL_ADDRESS,
                    DataObjectInterface::COL_LINK_TO_EVENT,
                    DataObjectInterface::COL_FREQUENCY,
                    DataObjectInterface::COL_DATE,
                    DataObjectInterface::COL_ADDRESS,
                    DataObjectInterface::COL_TIME,
                    DataObjectInterface::COL_LONGITUDE,
                    DataObjectInterface::COL_LATITUDE,
                    DataObjectInterface::COL_COUNTRY
                ],
                [
                    DataObjectInterface::COL_TOWN => 'Albertville',
                    DataObjectInterface::COL_NAME => 'Louise Collombier',
                    DataObjectInterface::COL_EMAIL_ADDRESS => 'yfc.albertville@gmail.com',
                    DataObjectInterface::COL_LINK_TO_EVENT => 'https://www.instagram.com/youth_for_climate_albertville/',
                    DataObjectInterface::COL_FREQUENCY => 'Once only',
                    DataObjectInterface::COL_DATE => '24/05/2019',
                    DataObjectInterface::COL_ADDRESS => 'Devant la mairie',
                    DataObjectInterface::COL_TIME => '14h',
                    DataObjectInterface::COL_LONGITUDE => '5° 27′ 14″ est',
                    DataObjectInterface::COL_LATITUDE => '43° 31′ 52″ nord,',
                    DataObjectInterface::COL_COUNTRY => 'France',
                ],
            ],
        ];
    }
}
