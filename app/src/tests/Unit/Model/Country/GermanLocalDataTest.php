<?php

namespace App\Tests\Unit\Model\Country;

use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Model\OverrideData;
use PHPUnit\Framework\TestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GermanLocalDataTest extends TestCase
{
    /**
     * @dataProvider getValidData
     */
    public function testBuildValues(array $data, array $colConfig, array $expectedResult)
    {
        /** @var OverrideData $overrideDataObject */
        $overrideDataObject = GermanLocalData::buildByOrderedArrayData($data, $colConfig);

        $this->assertSame($expectedResult, $overrideDataObject->getValuesByGeneralCols());
    }

    public function getValidData()
    {
        return [
            [
                [
                    '1234',
                    'fridaysforfuture@riseup.com',
                    '24/5/2019',
                    'Ansbach',
                    '13:00',
                    'Martin-Luther-Platz',
                    'Luca Salis',
                    '+491234',
                    'https://developersforfuture.org',
                ],
                [

                    DataObjectInterface::COL_TIMESTAMP,
                    DataObjectInterface::COL_EMAIL_ADDRESS,
                    DataObjectInterface::COL_DATE,
                    DataObjectInterface::COL_TOWN,
                    DataObjectInterface::COL_TIME,
                    DataObjectInterface::COL_ADDRESS,
                    DataObjectInterface::COL_NAME,
                    DataObjectInterface::COL_PHONE_NUMBER,
                    DataObjectInterface::COL_LINK_TO_EVENT,
                ],
                [
                    DataObjectInterface::COL_TIMESTAMP => '1234',
                    DataObjectInterface::COL_EMAIL_ADDRESS => 'fridaysforfuture@riseup.com',
                    DataObjectInterface::COL_DATE => '24/5/2019',
                    DataObjectInterface::COL_TOWN => 'Ansbach',
                    DataObjectInterface::COL_TIME => '13:00',
                    DataObjectInterface::COL_ADDRESS => 'Martin-Luther-Platz',
                    DataObjectInterface::COL_NAME => 'Luca Salis',
                    DataObjectInterface::COL_PHONE_NUMBER => '+491234',
                    DataObjectInterface::COL_LINK_TO_EVENT => 'https://developersforfuture.org',
                    DataObjectInterface::COL_COUNTRY => 'Germany'
                ],
            ],
        ];
    }
}
