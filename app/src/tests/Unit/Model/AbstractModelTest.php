<?php

namespace App\Tests\Unit\Model;

use App\Model\OverrideData;
use PHPUnit\Framework\TestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
abstract class AbstractModelTest extends TestCase
{

    /**
     * @dataProvider getValidData
     */
    public function testBuildValues(array $data, array $colConfig, array $expectedResult)
    {
        /** @var OverrideData $overrideDataObject */
        $overrideDataObject = OverrideData::buildByOrderedArrayData($data, $colConfig);

        $this->assertSame($expectedResult, $overrideDataObject->getValuesByGeneralCols());
    }
}
