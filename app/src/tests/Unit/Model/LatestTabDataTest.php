<?php

namespace App\Tests\Unit\Model;

use App\Model\DataObjectInterface;
use App\Model\LatestTabData;
use App\Model\OverrideData;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class LatestTabDataTest extends AbstractModelTest
{
    /**
     * @dataProvider getValidData
     */
    public function testBuildValues(array $data, array $colConfig, array $expectedResult)
    {
        /** @var OverrideData $overrideDataObject */
        $overrideDataObject = LatestTabData::buildByOrderedArrayData($data, $colConfig);

        $this->assertSame($expectedResult, $overrideDataObject->getValuesByGeneralCols());
    }

    public function getValidData()
    {
        return [
            [
                [
                    '123',
                    'info@developersforfuture.org',
                    'Developer',
                    '+4912345',
                    'Some Notes',
                    'Germany',
                    'Ansbach',
                    'Martin-Luther-Platz',
                    '14 Ur',
                    '24/05/2019',
                    'Once Only',
                    'https::/developersforfuture.org',
                ],
                [
                    DataObjectInterface::COL_TIMESTAMP,
                    DataObjectInterface::COL_EMAIL_ADDRESS,
                    DataObjectInterface::COL_NAME,
                    DataObjectInterface::COL_PHONE_NUMBER,
                    DataObjectInterface::COL_NOTES,
                    DataObjectInterface::COL_COUNTRY,
                    DataObjectInterface::COL_TOWN,
                    DataObjectInterface::COL_ADDRESS,
                    DataObjectInterface::COL_TIME,
                    DataObjectInterface::COL_DATE,
                    DataObjectInterface::COL_FREQUENCY,
                    DataObjectInterface::COL_LINK_TO_EVENT,
                ],
                [
                    DataObjectInterface::COL_TIMESTAMP => '123',
                    DataObjectInterface::COL_EMAIL_ADDRESS => 'info@developersforfuture.org',
                    DataObjectInterface::COL_NAME => 'Developer',
                    DataObjectInterface::COL_PHONE_NUMBER => '+4912345',
                    DataObjectInterface::COL_NOTES => 'Some Notes',
                    DataObjectInterface::COL_COUNTRY => 'Germany',
                    DataObjectInterface::COL_TOWN => 'Ansbach',
                    DataObjectInterface::COL_ADDRESS => 'Martin-Luther-Platz',
                    DataObjectInterface::COL_TIME => '14 Ur',
                    DataObjectInterface::COL_DATE => '24/05/2019',
                    DataObjectInterface::COL_FREQUENCY => 'Once Only',
                    DataObjectInterface::COL_LINK_TO_EVENT => 'https::/developersforfuture.org',
                ],
            ],
        ];
    }
}
