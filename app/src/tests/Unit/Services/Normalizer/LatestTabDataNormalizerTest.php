<?php

namespace App\Tests\Unit\Services\Normalizer;

use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Model\LatestTabData;
use App\Model\OverrideData;
use App\Services\Normalizer\GermanNormalizer;
use App\Services\Normalizer\LatestTabDataNormalizer;
use App\Services\Normalizer\OverrideDataNormalizer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class LatestTabDataNormalizerTest extends TestCase
{
    public function testResetData()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $normalizer = new LatestTabDataNormalizer($logger);
        $logger->expects($this->never())->method('warning');

        $result = $normalizer->normalize([['Town', 'Date'], ['Ansbach', '2019-05-29']], LatestTabData::class);
        $this->assertCount(1, $result);
        $dataObject = array_shift($result);
        $this->assertEquals('2019-05-29', $dataObject->date);
    }
}
