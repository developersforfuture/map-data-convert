<?php

namespace App\Tests\Unit\Services\Normalizer;

use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Services\Normalizer\GermanNormalizer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GermanNormalizerTest extends TestCase
{
    public function testResetData()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $normalizer = new GermanNormalizer($logger);
        $logger->expects($this->never())->method('warning');

        $result = $normalizer->normalize([['Stadt', 'Datum'], ['Ansbach', '29/05/2019']], GermanLocalData::class);
        $this->assertCount(1, $result);
        $dataObject = array_shift($result);
        $this->assertEquals('2019-05-29', $dataObject->date);
    }
}
