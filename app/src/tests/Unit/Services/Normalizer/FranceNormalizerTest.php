<?php

namespace App\Tests\Unit\Services\Normalizer;

use App\Model\Country\FranceLocalData;
use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Services\Normalizer\FranceNormalizer;
use App\Services\Normalizer\GermanNormalizer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class FranceNormalizerTest extends TestCase
{
    public function testResetData()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $normalizer = new FranceNormalizer($logger);
        $logger->expects($this->never())->method('warning');

        $result = $normalizer->normalize([['Ville', 'date'], ['Paris', '29/05/2019']], FranceLocalData::class);
        $this->assertCount(1, $result);
        $dataObject = array_shift($result);
        $this->assertEquals('2019-05-29', $dataObject->date);
    }
}
