<?php

use App\Services\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class UtilsTest extends TestCase
{
    /**
     * @dataProvider getHeadingData
     */
    public function testSeveralValidData(array $headData,array $expectedResult)
    {
        $colDefinition = [
            'col_1' => 'Col 1',
            'col_2' => 'Col 2',
            'col_3' => 'Col 3',
            'col_4' => 'Col 4',
        ];

        $actualResult = Utils::createHeadingByColDefinition($colDefinition, $headData);

        $this->assertSame($expectedResult, $actualResult);
    }

    public function getHeadingData(): array
    {
        return [
            'Sorted like col definition' => [
                [
                    'Col 1',
                    'Col 2',
                    'Col 3',
                    'Col 4',
                ],
                [
                    'col_1',
                    'col_2',
                    'col_3',
                    'col_4',
                ],
            ],
            'Not Sorted like col definition' => [
                [
                    'Col 1',
                    'Col 3',
                    'Col 4',
                    'Col 2',
                ],
                [
                    'col_1',
                    'col_3',
                    'col_4',
                    'col_2',
                ],
            ],
        ];
    }
}
