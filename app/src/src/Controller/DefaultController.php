<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class DefaultController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('base.html.twig', []);
    }
}
