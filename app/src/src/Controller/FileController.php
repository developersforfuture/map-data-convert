<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * http://map-data-convert.developersforfuture.org/file//app/tmpgerman_1558142660.csv
 *
 *
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class FileController extends AbstractController
{
    public function index($fileName): BinaryFileResponse
    {
        $outputFile = getenv('APP_BASEDIR_TMP').'/'.$fileName;
        if (!file_exists($outputFile)) {
            throw new NotFoundHttpException('File not found');
        }

        $response = new BinaryFileResponse($outputFile);

        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        if($mimeTypeGuesser->isSupported()){
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($outputFile));
        }else{
            $response->headers->set('Content-Type', 'text/csv');
        }

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );

        return $response;
    }
}
