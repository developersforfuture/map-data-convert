<?php

namespace App\Form;

use App\Model\GeneralData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GeneralDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('country', ChoiceType::class, ['choices' => ['Germany' => 'germany', 'France' => 'france', 'Denmark' => 'denmark', 'Italy' => 'italy']])
            ->add('localData', FileType::class, ['label' => 'Local data (CSV File)'])
            ->add('latestTabData', FileType::class, ['label' => 'Latest Tab Data (CSV File)'])
            ->add('overrideTabData', FileType::class, ['label' => 'Override Tab Data (CSV File)'])
            // ...
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => GeneralData::class]);
    }
}
