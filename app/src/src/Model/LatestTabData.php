<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @property string $name;
 * @property string $email;
 * @property string $frequency
 * @property string $timestamp
 *
 */
class LatestTabData extends AbstractDataObject
{
    protected static $COLS = [
        DataObjectInterface::COL_TIMESTAMP => 'Timestamp',
        DataObjectInterface::COL_EMAIL_ADDRESS => 'Email Address',
        DataObjectInterface::COL_NAME => 'Your name',
        DataObjectInterface::COL_PHONE_NUMBER => 'Phone',
        DataObjectInterface::COL_NOTES => 'Notes (optional)',
        DataObjectInterface::COL_COUNTRY => 'Country',
        DataObjectInterface::COL_TOWN => 'Town',
        DataObjectInterface::COL_ADDRESS => 'Address',
        DataObjectInterface::COL_TIME => 'Time',
        DataObjectInterface::COL_DATE => 'Date',
        DataObjectInterface::COL_FREQUENCY => 'Frequency',
        DataObjectInterface::COL_LINK_TO_EVENT => 'Link to event (URL), e.g. Facebook, Instagram, Twitter, web',
    ];

    protected function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface
    {
        $values = static::createValuesByCols($data, $config);

        return new self($values);
    }

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array
    {
        return static::$COLS;
    }
}
