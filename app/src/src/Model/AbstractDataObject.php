<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @property $date
 * @property $name
 * @property $town
 * @property $address
 */
abstract class AbstractDataObject implements DataObjectInterface
{

    /** @var array */
    protected $values;

    /**
     * Will output its data in an array with keys for the override or latest tab headers.
     *
     * @return array
     */
    public function getValuesByGeneralCols(): array
    {
        return $this->values;
    }

    /**
     * @param array $data
     * @param array $config
     *
     * @return array
     */
    protected static function createValuesByCols(array $data, array $config): array
    {
        $values = [];
        foreach ($config as $col => $headingColName) {
            if (!isset($data[$col])) {
                throw new \RuntimeException('No chance to create a record. Column with name '.$headingColName);
            }
            $values[$config[$col]] = trim($data[$col]);
        }

        return $values;
    }

    public function getHash(): string
    {
        if (!isset($this->values[DataObjectInterface::COL_TOWN]) || !isset($this->values[DataObjectInterface::COL_DATE]) ) {
            throw new \RuntimeException('You have to set town and date values to build a hash');
        }

        return md5($this->values[DataObjectInterface::COL_TOWN].'|'.$this->values[DataObjectInterface::COL_DATE]);
    }

    public function __get($name)
    {
        if (!isset($this->values[$name])) {
            return null;
        }

        return $this->values[$name];
    }
}
