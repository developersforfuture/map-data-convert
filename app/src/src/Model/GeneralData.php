<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GeneralData
{
    /**
     * @var string
     * @Assert\NotBlank(message="Please select your country.")
     * @Assert\NotBlank()
     */
    public $country;

    /**
     * @Assert\NotBlank(message="Please upload your data as csv file.")
     * @Assert\File(mimeTypes={ "text/csv" })
     */
    private $localData;

    /**
     * @Assert\NotBlank(message="Please upload latest tab data as csv file.")
     * @Assert\File(mimeTypes={ "text/csv" })
     */
    private $latestTabData;


    /**
     * @Assert\NotBlank(message="Please upload override data as csv file.")
     * @Assert\File(mimeTypes={ "text/csv" })
     */
    private $overrideTabData;

    /**
     * @return mixed
     */
    public function getLatestTabData()
    {
        return $this->latestTabData;
    }

    /**
     * @param mixed $latestTabData
     */
    public function setLatestTabData($latestTabData)
    {
        $this->latestTabData = $latestTabData;
    }

    /**
     * @return mixed
     */
    public function getLocalData()
    {
        return $this->localData;
    }

    /**
     * @param mixed $localData
     */
    public function setLocalData($localData)
    {
        $this->localData = $localData;
    }

    /**
     * @return mixed
     */
    public function getOverrideTabData()
    {
        return $this->overrideTabData;
    }

    /**
     * @param mixed $overrideTabData
     */
    public function setOverrideTabData($overrideTabData): void
    {
        $this->overrideTabData = $overrideTabData;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }
}
