<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
interface DataObjectInterface
{
    const COL_TIMESTAMP = 'timestamp';
    const COL_EMAIL_ADDRESS = 'email_address';
    const COL_NAME = 'name';
    const COL_REGISTRATION_CONSENT = 'registration_consent';
    const COL_SPOKESPERSON_CONSENT = 'spokesperson_consent';
    const COL_COUNTRY = 'country';
    const COL_TOWN = 'town';
    const COL_ADDRESS = 'address';
    const COL_TIME = 'time';
    const COL_DATE = 'date';
    const COL_FREQUENCY = 'frequency';
    const COL_ORGANIZATION = 'organization';
    const COL_PHONE_NUMBER = 'phone_number';
    const COL_NOTES = 'notes';
    const COL_EVENT_TYPE = 'event_type';
    const COL_LINK_TO_EVENT = 'link_to_event';
    const COL_EVENT_CONSENT = 'event_consent';
    const COL_APPROVE = 'approve';
    const COL_ORGANIZATION_TYPE = 'orga_type';
    const COL_COMMENT = 'comment';
    const COL_LOCALIZATION = 'localization';
    const COL_LATITUDE = 'lat';
    const COL_LONGITUDE = 'long';
    const COL_APPROVAL_TIME = 'approval_time';
    const COL_APPROVAL_EMAIL = 'approval_email';
    const COL_NUMBER_OF_PEOPLE = 'number_of_people';
    const COL_LEGAL_APPROVED = 'legal_apporved';

    /**
     * Will output its data in an array with keys for the override or latest tab headers.
     *
     * @return array
     */
    public function getValuesByGeneralCols(): array;

    /**
     * Creates an instance of the data object class by inserting ordered data.
     * Ordered means, the order we define in a kind of a config.
     *
     * @param array $data
     * @param array $config
     *
     * @return DataObjectInterface
     */
    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface;

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array;

    public function getHash(): string;
}
