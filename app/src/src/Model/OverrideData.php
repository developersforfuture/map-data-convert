<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class OverrideData extends AbstractDataObject
{
    protected static $COLS = [
        DataObjectInterface::COL_TIMESTAMP => 'Timestamp',
        DataObjectInterface::COL_EMAIL_ADDRESS => 'Email Address',
        DataObjectInterface::COL_NAME => 'Your name',
        DataObjectInterface::COL_REGISTRATION_CONSENT => 'Registration Consent',
        DataObjectInterface::COL_SPOKESPERSON_CONSENT => 'Spokesperson Consent',
        DataObjectInterface::COL_ORGANIZATION => 'Organization that you represent (optional)',
        DataObjectInterface::COL_PHONE_NUMBER => 'Phone number in international format (optional)',
        DataObjectInterface::COL_NOTES => 'Notes (optional)',
        DataObjectInterface::COL_EVENT_TYPE => 'Event Type',
        DataObjectInterface::COL_COUNTRY => 'Country',
        DataObjectInterface::COL_TOWN => 'Town',
        DataObjectInterface::COL_ADDRESS => 'Address',
        DataObjectInterface::COL_LINK_TO_EVENT => 'Link to event (URL), e.g. Facebook, Instagram, Twitter, web',
        DataObjectInterface::COL_FREQUENCY => 'Frequency',
        DataObjectInterface::COL_DATE => 'Date',
        DataObjectInterface::COL_TIME => 'Time',
        DataObjectInterface::COL_EVENT_CONSENT => 'Event Consent',
        DataObjectInterface::COL_APPROVE => 'Approve',
        DataObjectInterface::COL_ORGANIZATION_TYPE => 'OrgType',
        DataObjectInterface::COL_COMMENT => 'Comment',
        DataObjectInterface::COL_LOCALIZATION => 'Loc',
        DataObjectInterface::COL_LATITUDE => 'Lat',
        DataObjectInterface::COL_LONGITUDE => 'Lon',
        DataObjectInterface::COL_APPROVAL_TIME => 'Approval time',
        DataObjectInterface::COL_APPROVAL_EMAIL => 'Approver email',
        DataObjectInterface::COL_NUMBER_OF_PEOPLE => 'Number of people',
    ];

    private function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface
    {
        $values = static::createValuesByCols($data, $config);

        return new self($values);
    }

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array
    {
        return static::$COLS;
    }
}
