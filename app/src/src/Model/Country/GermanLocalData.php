<?php

namespace App\Model\Country;

use App\Model\AbstractDataObject;
use App\Model\DataObjectInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GermanLocalData extends AbstractDataObject
{
    const COUNTRY_NAME = 'Germany';
    protected static $COLS = [
        DataObjectInterface::COL_TIMESTAMP => 'Timestamp',
        DataObjectInterface::COL_EMAIL_ADDRESS => 'Email',
        DataObjectInterface::COL_DATE => 'Datum',
        DataObjectInterface::COL_TOWN => 'Stadt',
        DataObjectInterface::COL_TIME => 'Uhrzeit',
        DataObjectInterface::COL_ADDRESS => 'Adresse',
        DataObjectInterface::COL_LEGAL_APPROVED => 'Genehmigt',
        DataObjectInterface::COL_NAME => 'Name',
        DataObjectInterface::COL_PHONE_NUMBER => 'Telefon',
        DataObjectInterface::COL_COUNTRY => 'Land',
        DataObjectInterface::COL_LINK_TO_EVENT => 'URL',
        DataObjectInterface::COL_NOTES => 'Notes',
        DataObjectInterface::COL_NUMBER_OF_PEOPLE => 'Anzahl',
    ];

    protected function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface
    {
        $values = static::createValuesByCols($data, $config);
        if (!isset($values[self::COL_NAME]) || empty($values[self::COL_NAME])) {
            $values[self::COL_NAME] = 'Luca Salis';
        }
        if (!isset($values[self::COL_EMAIL_ADDRESS]) || empty($values[self::COL_EMAIL_ADDRESS])) {
            $values[self::COL_EMAIL_ADDRESS] = 'fridaysforfuture@riseup.com';
        }
        if (!isset($values[self::COL_COUNTRY]) || empty($values[self::COL_COUNTRY])) {
            $values[self::COL_COUNTRY] = self::COUNTRY_NAME;
        }

        return new self($values);
    }

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array
    {
        return static::$COLS;
    }
}
