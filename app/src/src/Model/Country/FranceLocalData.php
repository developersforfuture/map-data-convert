<?php

namespace App\Model\Country;

use App\Model\AbstractDataObject;
use App\Model\DataObjectInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class FranceLocalData extends AbstractDataObject
{
    const COUNTRY_NAME = 'France';
    protected static $COLS = [
        'carto_inter' => 'Carto inter',
        DataObjectInterface::COL_TOWN => 'Ville',
        DataObjectInterface::COL_TIMESTAMP => 'Timestamp',
        DataObjectInterface::COL_NAME => 'Nom Prénom',
        DataObjectInterface::COL_EMAIL_ADDRESS => 'Adresse Mail',
        DataObjectInterface::COL_LINK_TO_EVENT => 'URL',
        DataObjectInterface::COL_FREQUENCY => 'Frequence',
        DataObjectInterface::COL_DATE => 'date',
        DataObjectInterface::COL_ADDRESS => 'lieu ',
        DataObjectInterface::COL_TIME => 'heure',
        'zip' => 'Code postale',
        DataObjectInterface::COL_LONGITUDE => 'Longitude',
        DataObjectInterface::COL_LATITUDE => 'Latitude',
        'carto_fr' => 'Carto FR',
    ];


    protected function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface
    {
        $values = static::createValuesByCols($data, $config);

        if (!isset($values[self::COL_COUNTRY]) || empty($values[self::COL_COUNTRY])) {
            $values[self::COL_COUNTRY] = self::COUNTRY_NAME;
        }

        return new self($values);
    }

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array
    {
        return static::$COLS;
    }
}
