<?php

namespace App\Model\Country;

use App\Model\AbstractDataObject;
use App\Model\DataObjectInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class DanishLocalData extends AbstractDataObject
{
    const COUNTRY_NAME = 'Denmark';
    protected static $COLS = [
        DataObjectInterface::COL_TOWN => 'City',
        DataObjectInterface::COL_ADDRESS => 'Adress',
        DataObjectInterface::COL_TIME => 'Time',
        DataObjectInterface::COL_DATE => 'Date',
        DataObjectInterface::COL_NOTES => 'Note',
    ];

    protected function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function buildByOrderedArrayData(array $data, array $config): DataObjectInterface
    {
        $values = static::createValuesByCols($data, $config);
        if (!isset($values[self::COL_NAME]) || empty($values[self::COL_NAME])) {
            $values[self::COL_NAME] = 'Maximilian Berghoff';
        }
        if (!isset($values[self::COL_EMAIL_ADDRESS]) || empty($values[self::COL_EMAIL_ADDRESS])) {
            $values[self::COL_EMAIL_ADDRESS] = 'info@developersforfuture.org';
        }
        if (!isset($values[self::COL_COUNTRY]) || empty($values[self::COL_COUNTRY])) {
            $values[self::COL_COUNTRY] = self::COUNTRY_NAME;
        }

        return new self($values);
    }

    /**
     * A configuration to map local key into international keys. the local keys are the values, the international keys qre the keys again.
     *
     * @return string[]
     */
    public static function getColumns(): array
    {
        return static::$COLS;
    }
}
