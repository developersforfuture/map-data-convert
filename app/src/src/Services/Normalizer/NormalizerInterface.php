<?php

namespace App\Services\Normalizer;

use App\Model\DataObjectInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * A converter builds an array of general data object, which means special properties like i.e. date fields are normalized.
 */
interface NormalizerInterface
{
    /**
     * Each normalizer should handle one data class only.
     *
     * @param string $dataClass
     *
     * @return bool
     */
    public function supports(string $dataClass): bool;

    /**
     * Builds an array of data object instances, which holds data normalized with general keys.
     *
     * @param $data
     * @param string $dataClass
     *
     * @return DataObjectInterface[]
     */
    public function normalize(array $data, string $dataClass): array;
}
