<?php

namespace App\Services\Normalizer;

use App\Model\Country\GermanLocalData;
use App\Model\Country\ItalianLocalData;
use App\Model\DataObjectInterface;
use App\Services\Utils;
use IntlDateFormatter;
use Psr\Log\LoggerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ItalianNormalizer implements NormalizerInterface
{
    public const DATA_CLASS = ItalianLocalData::class;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Each normalizer should handle one data class only.
     *
     * @param string $dataClass
     *
     * @return bool
     */
    public function supports(string $dataClass): bool
    {
        return self::DATA_CLASS === $dataClass;
    }

    /**
     * Builds an array of data object instances, which holds data normalized with general keys.
     *
     * @param string[] $data
     * @param string $dataClass
     *
     * @return GermanLocalData[]
     */
    public function normalize(array $data, string $dataClass): array
    {
        $headingData = array_shift($data);
        $colDefinition = Utils::createHeadingByColDefinition(ItalianLocalData::getColumns(), $headingData);
        $result = [];
        foreach ($data as $row) {
            try {
                $row = $this->normalizeDate($colDefinition, $row);
            } catch (\RuntimeException $exception) {
                $this->logger->warning($exception->getMessage());
                continue;
            }
            $dataObject = $dataClass::buildByOrderedArrayData($row, $colDefinition);
            $result[] = $dataObject;
        }

        return $result;
    }

    /**
     * @param string[] $colDefinition
     * @param string [] $row
     *
     * @return mixed
     * @throws \Exception
     */
    private function normalizeDate(array $colDefinition, array $row)
    {
        $indexForDateCol = array_flip($colDefinition)[DataObjectInterface::COL_DATE];
        if (!isset($row[$indexForDateCol]) || empty($row[$indexForDateCol])) {
            return $row;
        }
        $date = $this->italianDateToUsual(trim($row[$indexForDateCol], ':'));
        $dateTime = date_create_from_format('d/m', $date, new \DateTimeZone('Europe/Berlin'));
        if (!$dateTime) {
            throw new \RuntimeException('Invalid Date format "' . $date . '" please use "d/m" as usual');
        }
        $row[$indexForDateCol] = $dateTime->format('Y-m-d');

        if (!isset($row[DataObjectInterface::COL_TIMESTAMP])) {
            $row[DataObjectInterface::COL_TIMESTAMP] = '';
        }
        return $row;
    }

    function italianDateToUsual($date)
    {
        $months = [
            '01' => 'gennaio',
            '02' => 'febbraio',
            '03' => 'marzo',
            '04' => 'aprile',
            '05' => 'maggio',
            '06' => 'giugno',
            '07' => 'luglio',
            '08' => 'agosto',
            '09' => 'settembre',
            '10' => 'ottobre',
            '11' => 'novembre',
            '12' => 'dicembre',
        ];

        $parts = explode(' ', $date);
        $dayNumber = '';
        $monthString = '';
        $dayString = '';
        if (2 === count($parts)) {
            list($dayNumber, $monthString) = $parts;
        } elseif (3 === count($parts)) {
            list($dayString, $dayNumber, $monthString) = $parts;
        }
        $flipped = array_flip($months);
        $monthNumber = '01';
        if (isset($flipped[strtolower($monthString)])) {
            $monthNumber = $flipped[strtolower($monthString)];
        }

        return $dayNumber . '/' . $monthNumber;
    }
}
