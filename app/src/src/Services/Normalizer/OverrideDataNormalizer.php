<?php

namespace App\Services\Normalizer;

use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Model\OverrideData;
use App\Services\Utils;
use Psr\Log\LoggerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class OverrideDataNormalizer implements NormalizerInterface
{
    public const DATA_CLASS = OverrideData::class;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Each normalizer should handle one data class only.
     *
     * @param string $dataClass
     *
     * @return bool
     */
    public function supports(string $dataClass): bool
    {
        return self::DATA_CLASS === $dataClass;
    }

    /**
     * Builds an array of data object instances, which holds data normalized with general keys.
     *
     * @param string[] $data
     * @param string $dataClass
     *
     * @return GermanLocalData[]
     */
    public function normalize(array $data, string $dataClass): array
    {
        $colDefinition = Utils::createHeadingByColDefinition(OverrideData::getColumns(), array_shift($data));
        $result = [];
        foreach ($data as $row) {
            $result[] = $dataClass::buildByOrderedArrayData($row, $colDefinition);
        }

        return $result;
    }

    /**
     * @param string[] $colDefinition
     * @param string []$row
     *
     * @return mixed
     * @throws \RuntimeException
     */
    private function normalizeDate(array $colDefinition, array $row)
    {
        $indexForDateCol = array_flip($colDefinition)[DataObjectInterface::COL_DATE];
        $dateTime = date_create_from_format('d/m/Y', $row[$indexForDateCol] , new \DateTimeZone('Europe/Berlin'));
        if (!$dateTime) {
            throw new \RuntimeException('Invalid Date format "' . $row[$indexForDateCol] . '" please use "DD/MM/YYYY" as usual');
        }
        $row[$indexForDateCol] = $dateTime->format('Y-m-d');

        return $row;
}
}
