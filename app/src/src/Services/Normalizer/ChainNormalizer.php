<?php

namespace App\Services\Normalizer;


use App\Services\Normalizer\NormalizerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ChainNormalizer implements NormalizerInterface
{
    /**
     * @var NormalizerInterface[]
     */
    private $normalizers;

    public function __construct()
    {
        $this->normalizers = [];
    }

    public function supports(string $dataClass): bool
    {
        return true; // chain does not need the supports method, but we keep the facade
    }

    /**
     * @param NormalizerInterface[] $data
     * @param string $dataClass
     *
     * @return array
     */
    public function normalize(array $data, string $dataClass): array
    {
        foreach ($this->normalizers as $normalizer) {
            if ($normalizer->supports($dataClass)) {
                return $normalizer->normalize($data, $dataClass);
            }
        }
    }

    public function addNormalizer(NormalizerInterface $normalizer): void
    {
        $this->normalizers[] = $normalizer;
    }
}
