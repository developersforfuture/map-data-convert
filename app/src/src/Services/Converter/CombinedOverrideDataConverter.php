<?php

namespace App\Services\Converter;

use App\Model\Country\GermanLocalData;
use App\Model\DataObjectInterface;
use App\Model\LatestTabData;
use App\Model\OverrideData;
use App\Services\Normalizer\ChainNormalizer;
use App\Services\Normalizer\DanishNormalizer;
use App\Services\Normalizer\FranceNormalizer;
use App\Services\Normalizer\GermanNormalizer;
use App\Services\Normalizer\ItalianNormalizer;
use App\Services\Normalizer\LatestTabDataNormalizer;
use App\Services\Normalizer\NormalizerInterface;
use App\Services\Normalizer\OverrideDataNormalizer;
use App\Services\Utils;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Tests\Compiler\D;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CombinedOverrideDataConverter
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var array
     */
    private $defaultValues;
    /**
     * @var NormalizerInterface|ChainNormalizer
     */
    private $normalizer;

    public function __construct(LoggerInterface $logger, NormalizerInterface $normalizer)
    {
        $this->logger = $logger;
        $this->normalizer = $normalizer;

        // Todo: move it into autowired tags
        $this->normalizer->addNormalizer(New GermanNormalizer($logger));
        $this->normalizer->addNormalizer(New FranceNormalizer($logger));
        $this->normalizer->addNormalizer(New OverrideDataNormalizer($logger));
        $this->normalizer->addNormalizer(New LatestTabDataNormalizer($logger));
        $this->normalizer->addNormalizer(New ItalianNormalizer($logger));
        $this->normalizer->addNormalizer(New DanishNormalizer($logger));
    }

    public function buildOverrideData(UploadedFile $localDataFile, UploadedFile $latestTabFile, UploadedFile $overrideTabFile, string $dataClass)
    {
        $overrideData = $this->readData($overrideTabFile);
        $latestTabData = $this->readData($latestTabFile);
        $localData = $this->readData($localDataFile);
        $overDataColConfiguration = Utils::createHeadingByColDefinition(OverrideData::getColumns(), $overrideData[0]);
        array_shift($latestTabData);

        /** @var LatestTabData $latestTabModels */
        $latestTabModels = $this->normalizer->normalize($latestTabData, LatestTabData::class);
        $overrideTabModels = $this->normalizer->normalize($overrideData, OverrideData::class);
        $localModels = $this->normalizer->normalize($localData, $dataClass);

        $overrideDataSortedByDate = [];
        /** @var GermanLocalData[] $localModels */
        $resultingOverrideData = [];
        foreach ($overrideTabModels as $model) {
            $overrideDataSortedByDate[$model->getHash()] = $model;
        }

        foreach ($localModels as $dataObject) {
            if (isset($overrideDataSortedByDate[$dataObject->getHash()])) {
                $this->logger->debug($dataObject->getHash().' Hash wurde gefunden.');
                $overrideDataSortedByDate[$dataObject->getHash()] = $this->mergeLocalDataIntoOverrideData(
                    $dataObject,
                    $overrideDataSortedByDate[$dataObject->getHash()],
                    $overDataColConfiguration
                );
            } else {
                $this->logger->debug($dataObject->getHash().' Hash nicht wurde gefunden.');
                $overrideDataSortedByDate[$dataObject->getHash()] = $this->createOverrideDataSetAwareOfLatestTabData(
                    $dataObject,
                    $latestTabModels,
                    $overDataColConfiguration
                );
            }
        }

        return $overrideDataSortedByDate;
    }

    /**
     * @param DataObjectInterface $localData
     * @param OverrideData $overrideDataObject
     * @param array $overrideColDefinition
     *
     * @return DataObjectInterface
     */
    private function mergeLocalDataIntoOverrideData(DataObjectInterface $localData, OverrideData $overrideDataObject, array $overrideColDefinition): DataObjectInterface
    {
        $overrideDataValues = $overrideDataObject->getValuesByGeneralCols();
        $localDataValues = $localData->getValuesByGeneralCols();
        $colKeys = array_merge(array_keys(LatestTabData::getColumns()), [DataObjectInterface::COL_NUMBER_OF_PEOPLE]);
        foreach ($colKeys as $key) {
            if (in_array($key, [DataObjectInterface::COL_TIMESTAMP])) {
                continue;
            }
            if (isset($localDataValues[$key]) && !empty($localDataValues[$key]) && $overrideDataValues[$key] !== $localDataValues[$key]) {
                $overrideDataValues[$key] = $localDataValues[$key];
            }
        }

        return OverrideData::buildByOrderedArrayData(array_values($overrideDataValues), $overrideColDefinition);
    }

    private function readData(UploadedFile $uploadedFile): array
    {
        $result = [];
        if (($handle = fopen($uploadedFile->getPath() . '/' . $uploadedFile->getFilename(), "r"))) {
            while (($data = fgetcsv($handle, 2000))) {
                $result[] = $data;
            }
        }

        return $result;
    }

    /**
     * @param DataObjectInterface $dataObject
     * @param LatestTabData[] $latestTabData
     *
     * @param array $overrideColDefinition
     *
     * @return DataObjectInterface
     */
    private function createOverrideDataSetAwareOfLatestTabData(DataObjectInterface $dataObject, array $latestTabData, array $overrideColDefinition): DataObjectInterface
    {
        $data = [];
        $dataObjectValues = $dataObject->getValuesByGeneralCols();
        foreach ($overrideColDefinition as $key) {
            $data[$key] = isset($dataObjectValues[$key]) ? $dataObjectValues[$key] : '';
        }

        if (isset($latestTabData[$dataObject->getHash()])) {
            $existingLatestTabDataObject = $latestTabData[$dataObject->getHash()];
            $data[DataObjectInterface::COL_FREQUENCY] = $existingLatestTabDataObject->frequnecy;
            $data[DataObjectInterface::COL_NAME] = $existingLatestTabDataObject->name;
            $data[DataObjectInterface::COL_EMAIL_ADDRESS] = $existingLatestTabDataObject->email;
            $data[DataObjectInterface::COL_TIMESTAMP] = $existingLatestTabDataObject->timestamp;
        }

        return OverrideData::buildByOrderedArrayData(array_values($data), $overrideColDefinition);
    }
}
