<?php

namespace App\Services;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class Utils
{
    public static function createHeadingByColDefinition($colDefinition, $headingData): array
    {
        $headingNamesWithGeneralCols = array_flip($colDefinition);
        $result = [];
        foreach ($headingData as $value) {
            if (isset($headingNamesWithGeneralCols[$value])) {
                $result[] = $headingNamesWithGeneralCols[$value];
            }
        }

        return $result;
    }
}
